import Vue from 'vue'
import VueRouter from 'vue-router'
//import HomeView from '../views/HomeView.vue'
import MainView from '../views/MainView.vue'
import TabOne from "@/components/One.vue";
import TabTwo from "@/components/Two.vue";
import TabThree from "@/components/Three.vue";

Vue.use(VueRouter)

const routes = [
  //{
    //path: '/',
    //name: 'home',
    //component: HomeView
  //},
  {
    path: '/',
    name: 'main',
    component: MainView,
    props: true,
    children: [
      {
        path: "/",
        component: TabOne
      },
      {
        path: "/one",
        component: TabOne
      },
      {
        path: "two",
        component: TabTwo
      },
      {
        path: "three",
        component: TabThree
      },
    ]
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
